CFEngine image

If you have 2 factor auth enabled you need an access token to make `docker login` work. The token
needs `write_registry`,`read_registry` and `read_api`(?) access. Use `docker login` with your user
name and the token as the password. This token MUST be a personal access token, not a project level
one.

Regen with:

* `docker build . -t registry.science.ru.nl/cncz/sys/image/cfengine:YYYY-MM-DD`
* `docker login registry.science.ru.nl/cncz/sys/image`
* `docker push registry.science.ru.nl/cncz/sys/image/cfengine:YYYY-MM-DD`
* `docker logout`

Then change the image tag used in the CFengine repository.

The gpg-pubkey comes from [sys/pkg](https://gitlab.science.ru.nl/cncz/sys/pkg). So, whenever that one is renewed (see [README over there](https://gitlab.science.ru.nl/cncz/sys/pkg#gpg-pubkey-updaten)), it should be distributed to this project. Ie:

```
cp cfengine/files/ubuntu/etc/apt/trusted.gpg.d/cncz-pages-keyring.gpg image/cfengine
```
